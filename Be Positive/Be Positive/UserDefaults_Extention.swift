//
//  UserDefaults_Extention.swift
//  Be Positive
//
//  Created by Trevor Noe on 9/25/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import Foundation
import UIKit

// extention to allow custom data objects to be saved and loaded
extension UserDefaults {
    
    func set(inventory: [PersonalityInventory], key: String) {
        guard let binaryData = try? NSKeyedArchiver.archivedData(withRootObject: inventory, requiringSecureCoding: false) else { fatalError("Error!") }
        self.set(binaryData, forKey: key)
    }
    
    func inventory(forKey key: String) -> [PersonalityInventory]? {
        if let binaryData = data(forKey: key) {
                guard let inventory = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(binaryData) as? [PersonalityInventory] else { fatalError("Problem unarchiving") }
                return inventory
            }
        
        return nil
    }
}
