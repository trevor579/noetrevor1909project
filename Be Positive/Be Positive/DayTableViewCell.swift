//
//  DayTableViewCell.swift
//  Be Positive
//
//  Created by Trevor Noe on 9/24/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import UIKit

class DayTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var wellnessLabel: UILabel!
    @IBOutlet weak var moodImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
