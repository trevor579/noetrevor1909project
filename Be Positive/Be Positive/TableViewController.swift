 //
//  TableViewController.swift
//  Be Positive
//
//  Created by Trevor Noe on 9/24/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import UIKit
import WatchConnectivity

class TableViewController: UITableViewController, WCSessionDelegate {

    var inventory = [PersonalityInventory]()

    var session: WCSession? {
        didSet {
            if let session = session {
                session.delegate = self
                session.activate()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        if WCSession.isSupported() {
            session = WCSession.default
        }
        
        // load saved data
        if let savedInventory = UserDefaults.standard.inventory(forKey: "inventory") {
            inventory = savedInventory
        }
        
        tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return inventory.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create custom cell, if it doesn't work, use default cell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell_id_1", for: indexPath) as? DayTableViewCell
            else {
                return tableView.dequeueReusableCell(withIdentifier: "cell_id_1", for: indexPath)
        }
        
        // get the object to display
        let cellObject = inventory[indexPath.row]

        // Configure the cell...
        cell.dayLabel.text = cellObject.day
        cell.wellnessLabel.text = cellObject.overallDay
        cell.moodImage.image = UIImage(named: cellObject.mood!)
        cell.moodImage.tintColor = cellObject.imageColor

        return cell
    }
    
    // MARK: WCSessionDelegate
    
    // get data from watch after new data created
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        
        if let data = message["watchInventory"] as? Data {
            NSKeyedUnarchiver.setClass(PersonalityInventory.self, forClassName: "PersonalityInventory")
            
            do {
                guard let inventoryArray = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? [PersonalityInventory] else {
                    fatalError("Whoops there was a problem unarchiving watch data")
                }
                
                // update data
                self.inventory = inventoryArray
                self.tableView.reloadData()
                
                // save data
                UserDefaults.standard.set(inventory: inventory, key: "inventory")
            } catch {
                fatalError("OH NO! PROBLEM WITH GETTING WATCH DATA")
            }
        }
    }
    
    // check to see if daily personality inventory has been done already
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        let date = Date()
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "EEEE, MMM d"
        let day = dayTimePeriodFormatter.string(from: date)
        
        for item in inventory {
            if item.day == day {
                
                let alert = UIAlertController(title: "Woah! Hold On!", message: "You can only do one Personality Inventory each day.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Got It", style: .default, handler: nil))
                
                present(alert, animated: true)
                
                return false
            }
        }
        return true
    }
 
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    // MARK: Navigation
    @IBAction func unwindFromPI(_ sender: UIStoryboardSegue) {
        if sender.source is PersonalityInventoryViewController {
            if let senderVC = sender.source as? PersonalityInventoryViewController {
                inventory.append(senderVC.inventory!)
                
                // save new data
                UserDefaults.standard.set(inventory: inventory, key: "inventory")
                
                // reload with new data
                tableView.reloadData()
            }
        }
    }
}
