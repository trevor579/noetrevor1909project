//
//  PersonalityInventoryViewController.swift
//  Be Positive
//
//  Created by Trevor Noe on 9/25/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import UIKit

class PersonalityInventoryViewController: UIViewController {

    @IBOutlet weak var questionNumberLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var bestBtn: UIButton!
    @IBOutlet weak var neutralBtn1: UIButton!
    @IBOutlet weak var neutralBtn2: UIButton!
    @IBOutlet weak var badBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    
    var inventory: PersonalityInventory?
    
    var questionNum: Int = 0
    
    var mood: String?
    var day: String?
    var productivity: Int = 0
    var activity: Int = 0
    var happiness: Int = 0
    var stress: Int = 0
    var steps: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        question0()
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, MMM d"
        day = formatter.string(from: date)
        
        //Set corner radius
        bestBtn.layer.cornerRadius = 10
        neutralBtn1.layer.cornerRadius = 10
        neutralBtn2.layer.cornerRadius = 10
        badBtn.layer.cornerRadius = 10

        
    }
    
    func nextQuestion() {
        
        if questionNum == 1 {
            question1()
        }
        else if questionNum == 2 {
            question2()
        }
        else if questionNum == 3 {
            question3()
        }
        else if questionNum == 4 {
            question4()
        }
        else { // Test over
            questionNumberLabel.text = ""
            questionLabel.text = "Save Results?"
            bestBtn.isHidden = true
            neutralBtn1.isHidden = true
            neutralBtn2.isHidden = true
            badBtn.isHidden = true
            saveBtn.isHidden = false
            
            inventory = PersonalityInventory(mood: mood!, day: day!, productivity: productivity, activity: activity, stress: stress, happiness: happiness, steps: steps)
        }
    }
    
    @IBAction func bestButtonPressed(_ sender: UIButton) {
        // question specific functionality
        switch questionNum {
        case 0: // how was your day
            mood = "Good"
        case 1: // did you exercise
            activity += 100
        case 2: // how productive
            productivity += 100
        case 3: // how stressed
            stress += 100
        case 4: // how happy
            happiness += 100
        default:
            print("Something went wrong")
        }
        // increase questionNum
        questionNum += 1
        
        // go to the next question
        nextQuestion()
    }
    
    @IBAction func neutralButton1Pressed(_ sender: UIButton) {
        // question specific functionality
        switch questionNum {
        case 0: // how was your day
            mood = "Okay"
        case 1: // did you exercise
            activity += 66
        case 2: // how productive
            productivity += 66
        case 3: // how stressed
            stress += 66
        case 4: // how happy
            happiness += 66
        default:
            print("Something went wrong")
        }
        questionNum += 1
        nextQuestion()
    }
    
    @IBAction func neutralButton2Pressed(_ sender: UIButton) {
        // question specific functionality
        switch questionNum {
        // case 0: button invisible for this quesiton
        case 1: // did you exercise
            activity += 33
        case 2: // how productive
            productivity += 33
        case 3: // how stressed
            stress += 33
        case 4: // how happy
            happiness += 33
        default:
            print("Something went wrong")
        }
        questionNum += 1
        nextQuestion()
    }
    
    @IBAction func badButtonPressed(_ sender: UIButton) {
        // question specific functionality
        switch questionNum {
        case 0: // how was your day
            mood = "Bad"
        case 1: // did you exercise
            activity += 0
        case 2: // how productive
            productivity += 0
        case 3: // how stressed
            stress += 0
        case 4: // how happy
            happiness += 0
        default:
            print("Something went wrong")
        }
        questionNum += 1
        nextQuestion()
    }
    
    
    // MARK: Question methods
    private func question0() {
        
        // question number
        questionNumberLabel.text = "Question 1"
        
        // Question
        questionLabel.text = "How was your day today?"
        
        // Answers
        bestBtn.setTitle("Good", for: .normal)
        neutralBtn1.setTitle("Okay", for: .normal)
        neutralBtn2.isHidden = true
        badBtn.setTitle("Bad", for: .normal)
    }
    
    private func question1() {
        
        questionNumberLabel.text = "Question 2"

        questionLabel.text = "Did you exercise today?"
        
        bestBtn.setTitle("I'm sore from all that activity", for: .normal)
        neutralBtn1.setTitle("I went for light jog", for: .normal)
        neutralBtn2.isHidden = false
        neutralBtn2.setTitle("I went for a walk",for: .normal)
        badBtn.setTitle("I didn't exercise today", for: .normal)
    }
    
    private func question2() {
        
        questionNumberLabel.text = "Question 3"

        questionLabel.text = "How productive were you today?"
        
        bestBtn.setTitle("I went above and beyond", for: .normal)
        neutralBtn1.setTitle("I finished what I had to", for: .normal)
        neutralBtn2.setTitle("I skimmed by", for: .normal)
        badBtn.setTitle("I should have done more", for: .normal)
    }
    
    private func question3() {
        
        questionNumberLabel.text = "Question 4"

        questionLabel.text = "How stressed were you today?"
        
        bestBtn.setTitle("I'm stress free", for: .normal)
        neutralBtn1.setTitle("Slightly stressed, but nothing major", for: .normal)
        neutralBtn2.setTitle("Stressed, but managable", for: .normal)
        badBtn.setTitle("I couldn't handle the stress", for: .normal)
    }
    
    private func question4() {
        
        questionNumberLabel.text = "Question 5"

        questionLabel.text = "How happy were you today?"
        
        bestBtn.setTitle("I felt like smiling all day", for: .normal)
        neutralBtn1.setTitle("I smiled once or twice", for: .normal)
        neutralBtn2.setTitle("I didn't smile, but I was okay", for: .normal)
        badBtn.setTitle("Not happy", for: .normal)
    }
}
