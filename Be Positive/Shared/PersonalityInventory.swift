//
//  PersonalityInventory.swift
//  Be Positive
//
//  Created by Trevor Noe on 9/17/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import Foundation
import UIKit

class PersonalityInventory: NSObject, NSCoding {
    
    // mood will be asked straight up
    var mood: String?
    
    // day of the week that inventory was performed
    var day: String? // day of week
    // var date: String? // full date
    
    // these will be based on scores that user won't see. Questions will add or subract to their score
    // higher is better for all
    var productivity: Int?
    var activity: Int?
    var happiness: Int?
    var stress: Int? // higher number means less stress
    
    // get from HealthKit api
    var steps: Int?
    
    // get overall day value
    var overallDayScore: Int {
        get {
            var average = productivity! + activity! + stress! + happiness!
            average /= 4
            return average
        }
    }
    
    // get overall day based on the average day score
    var overallDay: String {
        get {
            if overallDayScore < 33 { // score under 33 = bad day
                return "Bad Day"
            } else if overallDayScore >= 33 && overallDayScore < 66 { // score 33 or above and less than 66 = okay day
                return "Okay Day"
            } else { // score above 66 = good day
                return "Good Day"
            }
        }
    }
    
    // return color for mood image
    var imageColor: UIColor {
        get {
            switch mood {
            case "Good":
                return UIColor(named: "Mint") ?? UIColor.white
            case "Okay":
                return UIColor(named: "Maize") ?? UIColor.white
            case "Bad":
                return UIColor(named: "PastelRed") ?? UIColor.white
            default:
                print("A problem occurred getting mood color")
                return UIColor.white
            }
        }
    }
    
    init(mood: String, day: String, productivity: Int, activity: Int, stress: Int, happiness: Int, steps: Int) {
        self.mood = mood
        self.day = day
        self.productivity = productivity
        self.activity = activity
        self.stress = stress
        self.happiness = happiness
        self.steps = steps
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init(mood: "Bad", day: "Saturday", productivity: 0, activity: 0, stress: 0, happiness: 0, steps: 0)
        
        mood = aDecoder.decodeObject(forKey: "moodString") as? String
        day = aDecoder.decodeObject(forKey: "dayString") as? String
        productivity = aDecoder.decodeObject(forKey: "productivityInt") as? Int
        activity = aDecoder.decodeObject(forKey: "activityInt") as? Int
        stress = aDecoder.decodeObject(forKey: "stressInt") as? Int
        happiness = aDecoder.decodeObject(forKey: "happinessInt") as? Int
        steps = aDecoder.decodeObject(forKey: "stepsInt") as? Int
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(mood, forKey: "moodString")
        aCoder.encode(day, forKey: "dayString")
        aCoder.encode(productivity, forKey: "productivityInt")
        aCoder.encode(activity, forKey: "activityInt")
        aCoder.encode(stress, forKey: "stressInt")
        aCoder.encode(happiness, forKey: "happinessInt")
        aCoder.encode(steps, forKey: "stepsInt")
    }
}
