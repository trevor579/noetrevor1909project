//
//  DayRowController.swift
//  Be Positive WatchKit Extension
//
//  Created by Trevor Noe on 9/17/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import WatchKit

class DayRowController: NSObject {

    @IBOutlet weak var moodImage: WKInterfaceImage!
    @IBOutlet weak var dayLabel: WKInterfaceLabel!
    @IBOutlet weak var wellnessLabel: WKInterfaceLabel!
    
    // set up row
    var inventory: PersonalityInventory? {
        didSet {
            guard let inventory = inventory else { return }
            
            // Set image based on mood
            moodImage.setImage(UIImage(named: inventory.mood!))
            moodImage.setTintColor(inventory.imageColor)
            
            // figure out how good the day was
            wellnessLabel.setText(inventory.overallDay)
            
            // set day label
            dayLabel.setText(inventory.day) 
        }
    }
    
}
