//
//  PersonalityInventoryInterfaceController.swift
//  Be Positive WatchKit Extension
//
//  Created by Trevor Noe on 9/18/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import WatchKit
import Foundation
import HealthKit

protocol ModalPersonalityInventory {
    func didFinishInventory(inventory:PersonalityInventory)
}

class PersonalityInventoryInterfaceController: WKInterfaceController {
    
    @IBOutlet weak var questionLabel: WKInterfaceLabel!
    
    @IBOutlet weak var bestBtn: WKInterfaceButton!
    @IBOutlet weak var neutralBtn1: WKInterfaceButton!
    @IBOutlet weak var neutralBtn2: WKInterfaceButton!
    @IBOutlet weak var badBtn: WKInterfaceButton!
    
    //delegate to reference previous controller
    var delegate: InterfaceController?
    
    // keeps track of which quesiton is being asked
    var questionNum: Int = 0
    
    // variables that will be put into a new PersonalityInventory object.
    var mood: String?
    
    var day: String?
    
    var productivity: Int = 0
    var activity: Int = 0
    var happiness: Int = 0
    var stress: Int = 0
    
    var steps: Int = 0
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        self.delegate = context as? InterfaceController
        
        question0()
        
        // get the day of the week
        let date = Date()
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "EEEE, MMM d"
        day = dayTimePeriodFormatter.string(from: date)
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    func nextQuestion() {
        
        if questionNum == 1 {
            question1()
        }
        else if questionNum == 2 {
            question2()
        }
        else if questionNum == 3 {
            question3()
        }
        else if questionNum == 4 {
            question4()
        }
        else {
            let inventory = PersonalityInventory(mood: mood!, day: day!, productivity: productivity, activity: activity, stress: stress, happiness: happiness, steps: steps)
            self.delegate?.didFinishInventory(inventory: inventory)
            self.dismiss()
        }
    }
    
    // MARK: Button Actions
    
    @IBAction func bestButtonPressed() {
        
        // question specific functionality
        switch questionNum {
        case 0: // how was your day
            mood = "Good"
        case 1: // did you exercise
            activity += 100
        case 2: // how productive
            productivity += 100
        case 3: // how stressed
            stress += 100
        case 4: // how happy
            happiness += 100
        default:
            print("Something went wrong")
        }
        // increase questionNum
        questionNum += 1
        
        // go to the next question
        nextQuestion()
    }
    
    @IBAction func neutralButton1Pressed() {
        
        // question specific functionality
        switch questionNum {
        case 0: // how was your day
            mood = "Okay"
        case 1: // did you exercise
            activity += 66
        case 2: // how productive
            productivity += 66
        case 3: // how stressed
            stress += 66
        case 4: // how happy
            happiness += 66
        default:
            print("Something went wrong")
        }
        questionNum += 1
        nextQuestion()
    }
    
    @IBAction func neutralButton2Pressed() {
        // question specific functionality
        switch questionNum {
        // case 0: button invisible for this quesiton
        case 1: // did you exercise
            activity += 33
        case 2: // how productive
            productivity += 33
        case 3: // how stressed
            stress += 33
        case 4: // how happy
            happiness += 33
        default:
            print("Something went wrong")
        }
        questionNum += 1
        nextQuestion()
    }
    
    @IBAction func badButtonPressed() {
        // question specific functionality
        switch questionNum {
        case 0: // how was your day
            mood = "Bad"
        case 1: // did you exercise
            activity += 0
        case 2: // how productive
            productivity += 0
        case 3: // how stressed
            stress += 0
        case 4: // how happy
            happiness += 0
        default:
            print("Something went wrong")
        }
        questionNum += 1
        nextQuestion()
    }
    
    // MARK: Questons

    private func question0() {
        
        // Interface Controller title
        self.setTitle("Question 1")
        
        // Question
        questionLabel.setText("How was your day today?")
        
        // Answers
        bestBtn.setTitle("Good")
        neutralBtn1.setTitle("Okay")
        neutralBtn2.setHidden(true)
        badBtn.setTitle("Bad")
    }
    
    private func question1() {
        
        self.setTitle("Question 2")
        
        questionLabel.setText("Did you exercise today?")
        
        bestBtn.setTitle("I'm sore from all that activity")
        neutralBtn1.setTitle("I went for light jog")
        neutralBtn2.setHidden(false)
        neutralBtn2.setTitle("I went for a walk")
        badBtn.setTitle("I didn't exercise today")
    }
    
    private func question2() {
        
        self.setTitle("Question 3")
        
        questionLabel.setText("How productive were you today?")
        
        bestBtn.setTitle("I went above and beyond")
        neutralBtn1.setTitle("I finished what I had to")
        neutralBtn2.setTitle("I skimmed by")
        badBtn.setTitle("I should have done more")
    }
    
    private func question3() {
        
        self.setTitle("Question 4")
        
        questionLabel.setText("How stressed were you today?")
        
        bestBtn.setTitle("I'm stress free")
        neutralBtn1.setTitle("Slightly stressed, but nothing major")
        neutralBtn2.setTitle("Stressed, but managable")
        badBtn.setTitle("I couldn't handle the stress")
    }
    
    private func question4() {
        
        self.setTitle("Question 5")
        
        questionLabel.setText("How happy were you today?")
        
        bestBtn.setTitle("I felt like smiling all day")
        neutralBtn1.setTitle("I smiled once or twice")
        neutralBtn2.setTitle("I didn't smile, but I was okay")
        badBtn.setTitle("Not happy")
    }
    
}
