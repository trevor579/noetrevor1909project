//
//  InterfaceController.swift
//  Be Positive WatchKit Extension
//
//  Created by Trevor Noe on 9/17/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController, ModalPersonalityInventory, WCSessionDelegate {
    
    @IBOutlet weak var dayTable: WKInterfaceTable!
    
    var inventories = [PersonalityInventory]()
    
    
    // get data from iOS on start up
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        let myValues: [String:Any] = ["getInventory":true]
        
        if session.isReachable {
            
            session.sendMessage(myValues, replyHandler: {
                replyData in
                
                DispatchQueue.main.async {
                    if let data = replyData["newInventory"] as? Data {
                        NSKeyedUnarchiver.setClass(PersonalityInventory.self, forClassName: "PersonalityInventory")
                        
                        do {
                            guard let inventoryArray = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? [PersonalityInventory] else { fatalError("Can't get inventory array")}
                            
                            self.inventories = inventoryArray
                            self.tableRefresh()
                            
                        } catch {
                            fatalError("Can't unarchive data: \(error)")
                        }
                        
                    }
                }
                
            }, errorHandler: nil)
        }
    }
    
    // send data to iOS
    func sendMessage() {
    
        NSKeyedArchiver.setClassName("PersonalityInventory", for: PersonalityInventory.self)
        guard let data = try? NSKeyedArchiver.archivedData(withRootObject: inventories, requiringSecureCoding: false) else { fatalError("Hey! Sorry! Had a mishap archiving watch inventory") }
        
        let myValues: [String: Any] = ["watchInventory":data]
        
        if session!.isReachable {
            session?.sendMessage(myValues, replyHandler: nil, errorHandler: nil)
        }
        
    }
    
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        DispatchQueue.main.async {
            if (message["getWatchInventory"] as? Bool) != nil {
                NSKeyedArchiver.setClassName("PersonalityInventory", for: PersonalityInventory.self)
                
                guard let data = try? NSKeyedArchiver.archivedData(withRootObject: self.inventories, requiringSecureCoding: false) else { fatalError("Error archiving watch data") }
                replyHandler(["newWatchInventory" : data])
                
            }
        }
    }
    
    fileprivate let session: WCSession? = WCSession.isSupported() ? WCSession.default : nil
    
    override init() {
        super.init()
        session?.delegate = self
        session?.activate()
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        
        //tableRefresh()
        //setInventoryData()
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    func didFinishInventory(inventory:PersonalityInventory) {
        self.inventories.append(inventory)
        sendMessage()
        tableRefresh()
    }

    // update the table to show most up to date data
    func tableRefresh() {
        
        // set number of rows
        dayTable.setNumberOfRows(inventories.count, withRowType: "DayRow")
        
        for index in 0..<dayTable.numberOfRows {
            let row = dayTable.rowController(at: index) as! DayRowController
            row.inventory = inventories[index]
        }
    }
    
    // present details screen
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        let inventory = inventories[rowIndex]
        presentController(withName: "Details", context: inventory)
    }
    
    // just for testing. Remove before release
    func setInventoryData(){
        var inventoryArray = [PersonalityInventory]()
        
        let inventory1 = PersonalityInventory(mood: "Okay", day: "Monday", productivity: 35, activity: 50, stress: 22, happiness: 60, steps: 1000)
        let inventory2 = PersonalityInventory(mood: "Good", day: "Tuesday", productivity: 80, activity: 75, stress: 75, happiness: 75, steps: 1000)
        let inventory3 = PersonalityInventory(mood: "Bad", day: "Wednesday", productivity: 10, activity: 10, stress: 15, happiness: 20, steps: 1000)
        let inventory4 = PersonalityInventory(mood: "Okay", day: "Thursday", productivity: 35, activity: 50, stress: 50, happiness: 50, steps: 1000)
        let inventory5 = PersonalityInventory(mood: "Good", day: "Friday", productivity: 80, activity: 70, stress: 15, happiness: 80, steps: 1000)
        
        inventoryArray = [inventory1, inventory2, inventory3, inventory4, inventory5]
        
        inventories = inventoryArray
    }
    
    
    // present personality inventory screen
    @IBAction func goToPersonalityInventory() {
        
        // only one personality inventory can be done in a day
        // get current date
        let date = Date()
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "EEEE, MMM d"
        let day = dayTimePeriodFormatter.string(from: date)
        
        // make sure a personality inventory hasn't been completed today
        for inventory in inventories {
            if day == inventory.day {
                let handler = { return }
                // alert user why the button doesn't work
                presentAlert(withTitle: "Woah! Hold On!", message: "You can only do one Personality Inventory each day.", preferredStyle: .alert, actions: [WKAlertAction.init(title: "Got It", style: .default, handler: handler)])
            }
        }
    
        self.presentController(withName: "PersonalityController", context: self)
    }
}

