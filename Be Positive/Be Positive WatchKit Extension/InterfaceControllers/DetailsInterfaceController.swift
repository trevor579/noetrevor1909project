//
//  DetailsInterfaceController.swift
//  Be Positive WatchKit Extension
//
//  Created by Trevor Noe on 9/18/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import WatchKit
import Foundation


class DetailsInterfaceController: WKInterfaceController {

    @IBOutlet weak var dayLabel: WKInterfaceLabel!
    @IBOutlet weak var moodLabel: WKInterfaceLabel!
    @IBOutlet weak var happinessLabel: WKInterfaceLabel!
    @IBOutlet weak var activityLabel: WKInterfaceLabel!
    @IBOutlet weak var stressLabel: WKInterfaceLabel!
    @IBOutlet weak var stepsLabel: WKInterfaceLabel!
    @IBOutlet weak var overallLabel: WKInterfaceLabel!
    
    var inventory: PersonalityInventory? {
        
        // Set up UI
        didSet {
            guard let inventory = inventory else { return }
            
            self.setTitle("Back")
            moodLabel.setText("Mood: \(inventory.mood!)")
            overallLabel.setText(inventory.overallDay)
            dayLabel.setText(inventory.day)
            
            
            // activity score
            if inventory.activity! < 33 {
                activityLabel.setText("Not active")
            } else if inventory.activity! >= 33 && inventory.activity! < 66 {
                activityLabel.setText("Somewhat active")
            } else {
                activityLabel.setText("Very active")
            }
            
            
            // happiness score
            if inventory.happiness! < 33 {
                happinessLabel.setText("Unhappy")
            } else if inventory.happiness! >= 33 && inventory.happiness! < 66 {
                happinessLabel.setText("Somewhat happy")
            } else {
                happinessLabel.setText("Very happy")
            }
            
            // stress score
            if inventory.stress! < 33 {
                stressLabel.setText("Very stressed")
            } else if inventory.stress! >= 33 && inventory.stress! < 66 {
                stressLabel.setText("Somewhat stressed")
            } else {
                stressLabel.setText("No stress")
            }
            
            stepsLabel.setText("\(inventory.steps?.description ?? 0.description) Steps")
        }
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        if let inventory = context as? PersonalityInventory {
            self.inventory = inventory
        }
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
