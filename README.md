# Be Positive

Be Positive is an app dedicated to helping you feel more positive. To do this, you'll be asked a series of questions with multiple 
choice answers that end up giving you a result that should be indicative of your overall day. The results will show you your overall activity,
productivity, stress, and happiness levels. This way you can see trends in your overall mood with your behaviors. 

